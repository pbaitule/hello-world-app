'use strict';

const express = require('express');
const path = require('path');
const client = require('prom-client');
const ResponseTime = require('response-time');

const collectDefaultMetrics = client.collectDefaultMetrics;

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

const numOfRequests = new client.Counter({  
    name: 'numOfRequests',
    help: 'Number of requests made',
    labelNames: ['method']
});

const requestCounters = function (req, res, next) {  
    if (req.path != '/webapp/metrics') {
        numOfRequests.inc({ method: req.method });
    }
    next();
};

const responses = new client.Summary({  
    name: 'responses',
    help: 'Response time in millis',
    labelNames: ['method', 'path', 'status']
});

const responseCounters = ResponseTime(function (req, res, time) {  
    console.log(req.url);
    if(req.path != '/webapp/metrics') {
        responses.labels(req.method, req.url, res.statusCode).observe(time);
    }
});
app.use(requestCounters);
app.use(responseCounters);

app.get('/webapp', (req, res, next) => {
    res.render('index', {title: 'k8s feeling gr8', message: 'Hello World!!!'});
});
app.get('/webapp/metrics', (request, response) => {
    response.set('Content-Type', client.register.contentType);
    response.send(client.register.metrics());
  });

collectDefaultMetrics();
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);